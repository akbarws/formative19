package com.akbar.formative19.controller;

import java.util.Scanner;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.akbar.formative19.view.ApplicationView;

@Component
public class ApplicationController {
	
	@Autowired
	AuthorController authorController;
	@Autowired
	BookController bookController;
	@Autowired
	ApplicationView view;
	
	Scanner scan = new Scanner(System.in);

	public void start() {
		boolean isExit = false;
		do {
			view.menu();
			String inputMenu = inputMenu();
			switch (inputMenu) {
			case "1":
				authorController.getAll();
				break;
			case "2":
				authorController.findById();
				break;
			case "3":
				bookController.getAll();
				break;
			case "4":
				bookController.findById();
				break;
			case "5":
				bookController.getBooksFromAuthor();
				break;
			default:
				isExit = true;
				break;
			}
		} while (!isExit);
	}
	
	private String inputMenu() {
		String inputMenu = "";
		boolean isValid = false;
		String regexMenu = "[1-5]{1}";
		do {
			System.out.print("Input option : ");
			inputMenu = scan.nextLine();
			if(Pattern.matches(regexMenu, inputMenu))
				isValid = true;
		} while (!isValid);
		return inputMenu;
	}
}
