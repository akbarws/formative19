package com.akbar.formative19.controller;

import java.util.Scanner;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.akbar.formative19.repository.AuthorRepository;
import com.akbar.formative19.view.AuthorView;

@Component
public class AuthorController {

	@Autowired
	AuthorRepository repository;
	@Autowired
	AuthorView view;
	
	Scanner scan = new Scanner(System.in);
	
	public void findById() {
		String id = inputId();
		if(isExistId(id))
			view.print(repository.findById(Integer.valueOf(id)));
		else
			view.print("Not Found");
	}
	
	private boolean isExistId(String inputId) {
		int count = repository.countById(Integer.valueOf(inputId));
		return (count > 0) ? true : false;
	}

	private String inputId() {
		String id="";
		String regexId = "^[1-9][0-9]*$";
		boolean isValid = false;
		do {
			System.out.print("Input ID Author : ");
			id = scan.nextLine();
			if(Pattern.matches(regexId, id))
				isValid = true;
		} while (!isValid);
		return id;
	}

	public void getAll(){
		view.print(repository.getAll());
	}
}
