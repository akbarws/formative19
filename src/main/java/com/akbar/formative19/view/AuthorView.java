package com.akbar.formative19.view;

import java.util.List;

import org.springframework.stereotype.Component;

import com.akbar.formative19.entity.Author;

@Component
public class AuthorView {

	public void print(Author author) {
		System.out.println("author [id=" + author.getId() + ", name=" + author.getName() + "]");
	}

	public void print(List<Author> authors) {
		System.out.println("#######################");
		if(authors.size()>0) {
			System.out.println("List of Authors");
			authors.forEach(author -> {
				System.out.println("author [id=" + author.getId() + ", name=" + author.getName() + "]");			
			});			
		} else {
			System.out.println("Empty");
		}
		System.out.println("#######################");
	}

	public void print(String message) {
		System.out.println("#######################");
		System.out.println(message);
		System.out.println("#######################");
	}

}
