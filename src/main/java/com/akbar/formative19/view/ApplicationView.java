package com.akbar.formative19.view;

import org.springframework.stereotype.Component;

@Component
public class ApplicationView {

	public void menu() {
		print(new String[] {"Author", "1. Get All Authors", "2. Find Author By Id", "Book", "3. Get All Books", "4. Find Book By Id", "5. Get Books From Authors"});
	}
	
	private void print(String[] menus) {
		for(String menu : menus) {
			System.out.println(menu);
		}
	}
}
