package com.akbar.formative19.view;

import java.util.List;

import org.springframework.stereotype.Component;

import com.akbar.formative19.entity.Book;

@Component
public class BookView {

	public void print(Book book) {
		System.out.println("Book [id=" + book.getId() + ", name=" + book.getName() + ", author_id=" + book.getAuthor_id() + "]");
	}

	public void print(List<Book> books) {
		System.out.println("#######################");
		if(books.size() > 0) {
			System.out.println("List of Books");
			books.forEach(book -> {
				System.out.println("Book [id=" + book.getId() + ", name=" + book.getName() + ", author_id=" + book.getAuthor_id() + "]");			
			});			
		} else {
			System.out.println("Empty");
		}
		System.out.println("#######################");
	}

	public void print(String message) {
		System.out.println("#######################");
		System.out.println(message);
		System.out.println("#######################");
	}

	public void print(Object[] bookName, String authorName) {
		System.out.println("#######################");
		System.out.println("List of Book from "+authorName);
		for(Object name : bookName) {
			System.out.println(name);
		}
		System.out.println("#######################");
	}

}
