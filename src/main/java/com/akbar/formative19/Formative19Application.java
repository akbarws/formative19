package com.akbar.formative19;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.akbar.formative19.controller.ApplicationController;

@SpringBootApplication
public class Formative19Application implements CommandLineRunner {

	@Autowired
	ApplicationController applicationController;
	
	public static void main(String[] args) {
		SpringApplication.run(Formative19Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		applicationController.start();
	}

}
