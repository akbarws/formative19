package com.akbar.formative19.repository;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.akbar.formative19.entity.Book;

@Repository
public class BookRepository {

	@PersistenceContext
	EntityManager entityManager;
	
	public Book findById(int id) {
		return entityManager.createQuery("SELECT b FROM Book b WHERE id="+id, Book.class).getSingleResult();
	}
	
	public List<Book> getAll() {
		return entityManager.createQuery("SELECT b FROM Book b", Book.class).getResultList();
	}

	public int countById(int id) {
		Query q = entityManager.createNativeQuery("SELECT count(*) FROM Book WHERE id="+id);
		return ((BigInteger) q.getSingleResult()).intValue();
	}

	public int countByAuthorName(String authorName) {
		String sql = "SELECT count(*) FROM Book JOIN Author ON Book.author_id = Author.id WHERE Author.name='"+authorName+"'";
		Query q = entityManager.createNativeQuery(sql);
		return ((BigInteger) q.getSingleResult()).intValue();
	}

	public List<String> getBooksFromAuthorName(String author_name) {
		String sql = "SELECT b.name AS BOOK_NAME FROM Book b JOIN Author a ON b.author_id = a.id WHERE a.name = '"+author_name+"'";
		TypedQuery<String> q = entityManager.createQuery(sql, String.class);
		return q.getResultList();
	}
		
}
