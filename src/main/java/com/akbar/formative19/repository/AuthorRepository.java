package com.akbar.formative19.repository;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.akbar.formative19.entity.Author;

@Repository
public class AuthorRepository {

	@PersistenceContext
	EntityManager entityManager;
	
	public Author findById(int id) {
		return entityManager.createQuery("SELECT a FROM Author a WHERE id="+id, Author.class).getSingleResult();
	}
	
	public List<Author> getAll() {
		return entityManager.createQuery("SELECT a FROM Author a", Author.class).getResultList();
	}

	public int countById(int id) {
		Query q = entityManager.createNativeQuery("SELECT count(*) FROM Author WHERE id="+id);
		return ((BigInteger) q.getSingleResult()).intValue();
	}
}
